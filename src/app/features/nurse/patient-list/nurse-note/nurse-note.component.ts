
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-nurse-note',
  templateUrl: './nurse-note.component.html',
  styleUrls: ['./nurse-note.component.css']
})
export class NurseNoteComponent {

  queryParamsData: any;


  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    


  ) {
    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
  }

}
