import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
// import { NurseService } from '../../services/nurse.service';
import { DateTime } from 'luxon';
import { AxiosResponse } from 'axios';
import { LibService } from '../../../shared/services/lib.service';
import { UserProfileService } from '../../../core/services/user-profiles.service';
import * as _ from 'lodash';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent {

  jwtHelper: JwtHelperService = new JwtHelperService();

  dataWard: any;
  wardName: any;
  wards: any = [];
  query: any;
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;
  isVisible = false;
  userId: any = '';
  totalInfo: any;

  constructor(
    private router: Router,
    private libService: LibService,
    private message: NzMessageService,
    private modal: NzModalService,
    private userProfileService: UserProfileService
  ) {
    const token: any = sessionStorage.getItem('token');
    const decoded: any = this.jwtHelper.decodeToken(token);
    this.userId = decoded.sub;
  }

  async ngOnInit() {
    // this.user_login_name  =  this.userProfileService.user_login_name;
    this.user_login_name = sessionStorage.getItem('userLoginName');

    console.log(this.user_login_name);
    // this.getWard();
    this.getList();
  }

  async getTotalInfo() {
    // const response: any = await this.nurseService.getPatientByHn('');
    // this.totalInfo = response.data.total;
  }

  async doSearch() {
    // this.getList();
    const messageId = this.message.loading('Loading...').messageId;
    try {
      // const response = await this.nurseService.getPatientByHn(this.query);
      // console.log(response.data);
      // const data: any = response.data;
      // this.total = data.total || 1
      // this.dataSet = data.data.map((v: any) => {
      //   const date = v.create_date ? DateTime.fromISO(v.create_date).setLocale('th').toLocaleString(DateTime.DATE_MED) : '';
      //   const ordate = v.ordate ? DateTime.fromISO(v.ordate).setLocale('th').toLocaleString(DateTime.DATE_MED) : '';
      //   v.admit_date = date;
      //   v.ordate = ordate;
      //   return v;
      // });
      this.message.remove(messageId);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }
  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }


  onPageIndexChange(pageIndex: any) {

    this.offset = pageIndex === 1 ?
      0 : (pageIndex - 1) * this.pageSize;

    this.getList()
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize
    this.pageIndex = 1

    this.offset = 0

    this.getList()
  }

  refresh() {
    this.query = '';
    this.pageIndex = 1;
    this.offset = 0;
    this.getList();
  }

  async onSelectWard(event: any) {
    console.log(event);

    this.dataWard = event;
    this.getList();
    this.wardName = event.name;
  }

  async getList() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      // const response = await this.nurseService.getPatient();
      // const data: any = response.data;
      // console.log(data);
      // this.total = data.total || 1
      // this.dataSet = data.data.map((v: any) => {
      //   const date = v.create_date ? DateTime.fromISO(v.create_date).setLocale('th').toLocaleString(DateTime.DATE_MED) : '';
      //   const ordate = v.ordate ? DateTime.fromISO(v.ordate).setLocale('th').toLocaleString(DateTime.DATE_MED) : '';
      //   v.create_date = date;
      //   v.ordate = ordate;
      //   return v;
      // });
      this.getTotalInfo();
      this.message.remove(messageId);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }

  nurseNote(data: any) {
    console.log(data);
    let dk: any = { an: data.an };
    let jsonString = JSON.stringify(dk);
    console.log(jsonString);
    this.router.navigate(['/nurse/patient-list/nurse-note'], { queryParams: { data: jsonString } });

  }

  viewUser(i: any) {
    // this.mdlViewUser.showModal(i);
  }

  setPasswordPatient(user: any): void {
    // this.mdlChangePassword.showModal(user);
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  onSubmitRegister(event: any) {
    if (event) {
      this.getList()
    }
  }

  async openNewUserRegister() {
    // this.mdlNewUser.showModal()
  }

  async openEdit(data: any) {
    // this.mdlNewUser.showModal(data)
  }

  cancel() {
    console.log('Cancelled');
    this.getList();
  }

  async doChangeStatus(i: any) {
    console.log(i);
    const messageId = this.message.loading('กำลังบันทึกข้อมูล...', { nzDuration: 0 }).messageId
    let data: any = {
      'is_active': false
    }
    try {
      // await this.nurseService.update(i.id, data)
      this.message.remove(messageId)
      this.message.success('ดำเนินการเสร็จเรียบร้อย')
      this.isVisible = false
      this.getList()
    } catch (error: any) {
      this.message.remove(messageId)
      this.message.error(`${error.code} - ${error.message}`)
    }
  }

}
