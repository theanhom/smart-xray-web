import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { XrayComponent } from './xray.component';

const routes: Routes = [
  {
    path: '',
    component: XrayComponent,
    children: [
      {
        path: '', redirectTo: 'patient-list', pathMatch: 'full'
      },
      {
        path: 'patient-list',
        loadChildren: () => import('./patient-list/patient-list.module').then(m => m.PatientListModule),
        data: {
          breadcrumb: 'ทะเบียนผู้ป่วย'
        },
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class XrayRoutingModule { }
