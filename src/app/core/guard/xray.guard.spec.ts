import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { xrayGuard } from './xray.guard';

describe('xrayGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) => 
      TestBed.runInInjectionContext(() => xrayGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
