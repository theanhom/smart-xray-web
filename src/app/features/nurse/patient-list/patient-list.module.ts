import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientListRoutingModule } from './patient-list-routing.module';
import { PatientListComponent } from './patient-list.component';
import { MainComponent } from './main/main.component';
import { NgZorroModule } from '../../../ng-zorro.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChangeWardComponent } from './change-bed/change-ward.component';
import { NurseNoteComponent } from './nurse-note/nurse-note.component';
import { ModalChangePasswordComponent } from '../patient-list/modals/modal-change-password/modal-change-password.component';
import { ModalNewUserComponent } from './modals/modal-new-user/modal-new-user.component';
import { ModalViewUserComponent } from './modals/modal-view-user/modal-view-user.component';

@NgModule({
  declarations: [
    PatientListComponent, MainComponent,
    ChangeWardComponent,
    NurseNoteComponent,
    ModalChangePasswordComponent,
    ModalNewUserComponent,
    ModalViewUserComponent
  ],
  imports: [
    CommonModule,
    NgZorroModule,
    FormsModule,
    ReactiveFormsModule,
    PatientListRoutingModule
  ]
})
export class PatientListModule { }
