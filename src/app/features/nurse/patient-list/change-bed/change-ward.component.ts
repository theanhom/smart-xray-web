import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { DateTime } from 'luxon';
import { NurseService } from '../../services/nurse.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { UserProfileService } from '../../../../core/services/user-profiles.service';
import { AxiosResponse } from 'axios';
import { LibService } from 'src/app/shared/services/lib.service';
import * as _ from 'lodash';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { NgxSpinnerService, Spinner } from "ngx-spinner";
import { NotificationService } from '../../../../shared/services/notification.service';
import { CanComponentDeactivate } from '../../../../core/guard/can-deactivate.guard'



@Component({
  selector: 'app-change-ward',
  templateUrl: './change-ward.component.html',
  styleUrls: ['./change-ward.component.css']
})
export class ChangeWardComponent implements OnInit, CanComponentDeactivate {
  // confirmModal?: NzModalRef;
  confirmModal?: NzModalRef<any, any> | undefined;
  query: any = '';
  dataSet: any[] = [];
  dataSetReview: any[] = [];
  dataSetAdmit: any[] = [];

  dataSetTreatement: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;

  wardId: any;
  doctorId: any;
  wards: any = [];
  beds: any = [];
  doctors: any = [];
  queryParamsData: any;
  doctorBy: any;
  preDiag: any;
  chieft_complaint: any;
  panelsWard: any[] = [];
  panelsBed: any[] = [];
  bedId: any;
  isLoading: boolean = true;
  isVisible = false;
  userId: any;
  departmentId: any;
  isSaved = false;
  //private routeSub: any;  // subscription to route observer
  wardTags: any[] = [];
  bedTags: any[] = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private nurseService: NurseService,
    private spinner: NgxSpinnerService,
    private nzMessageService: NzMessageService,
    private modal: NzModalService,
    private libService: LibService,
    private userProfileService: UserProfileService,
    private notificationService: NotificationService


  ) {

    this.user_login_name = this.userProfileService.fname;
    this.userId = sessionStorage.getItem('userID');
    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    console.log(this.queryParamsData);


  }

  ngOnInit() {

    this.getWard();
    this.getDoctor();

    this.assignCollapseWard();
  }

  /////////////เมธอด หลัก///////////ที่ต้องมี///////////////
  async canDeactivate() {
    if (!this.isSaved) {
      const confirm = await this.showConfirmModal('คำชี้แจ้ง', 'คุณยังไม่ได้บันทึกข้อมูล คุณแน่ใจว่าจะยกเลิกกระบวนการนี้ !');
      if (confirm) {
        this.isSaved = true;
        return true;
      } else {
        return false;
      }
    }
    return true;
  }

  showConfirmModal(title: string, content: string): Promise<boolean> {
    return new Promise<boolean>((resolve) => {
      this.confirmModal = this.modal.confirm({
        nzTitle: title,
        nzContent: content,
        nzOnOk: () => {
          resolve(true);
        },
        nzOnCancel: () => {
          resolve(false);
        }
      });
    });
  }

  hideSpinner() {
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
  }


  cancel(): void {
    this.nzMessageService.info('click cancel');
  }

  confirm(): void {
    this.nzMessageService.info('click confirm');
  }

  beforeConfirm(): Promise<boolean> {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(true);
      }, 3000);
    });
  }

  //////////////////////






  async getWard() {
    try {
      const response: AxiosResponse = await this.libService.getWard();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.wards = data;
      console.log('Ward : ', this.wards);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');

    }
  }

  async getDoctor() {
    try {
      const response: AxiosResponse = await this.libService.getDoctor();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.doctors = data;
      if (!_.isEmpty(data)) {
        // this.doctorId = .id;
      }
      console.log('Doctor : ', this.doctors);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');

    }
  }

  assignCollapseWard() {
    this.panelsWard = [
      {
        active: true,
        name: 'เลือกตึก',
        disabled: false
      },
    ];
  }

  assignCollapseBed() {
    this.panelsBed = [
      {
        active: true,
        name: 'เลือกเตียง',
        disabled: false
      },
    ];
  }

  setWard(data: any) {
    console.log(data);

    this.wardId = data.id;
    let wardName = data.name;
    this.departmentId = data.department_id;
    this.panelsWard = [
      {
        active: false,
        name: 'เลือกตึก : ' + wardName,
        disabled: false
      },
    ];
    console.log('wardId_now : ', this.wardId);
    this.handleInputConfirm(wardName);
    this.bedId = '';
    this.getBed(this.wardId);
  }

  async getBed(wardId: any) {
    try {
      console.log('wardsId : ', wardId);
      const response: AxiosResponse = await this.libService.getBed(wardId);
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.beds = data;
      this.assignCollapseBed();
      console.log('Beds : ', this.beds);
    } catch (error: any) {
      console.log(error);
      this.notificationService.notificationError('คำชี้แจ้ง', 'พบข้อผิดพลาด..' + error, 'top');


    }
  }

  setBed(data: any) {
    console.log('dataBeds : ', data);

    this.bedId = data.bed_id;
    let bedName = data.name;
    this.panelsBed = [
      {
        active: false,
        name: 'เลือกเตียง : ' + bedName,
        disabled: false
      },
    ];
    this.handleInputConfirmBed(bedName);
    console.log('bedId : ', this.bedId);
  }

  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }

  async saveRegister() {

  }




  handleOk(): void {
    this.isVisible = false;
    this.saveRegister();

  }

  handleCancel(): void {
    console.log('Cancel Save');
  }



  async navigatePatientList() {

    this.router.navigate(['/nurse/patient-list']);

  }

  /////////////////get data  connter here/////////////////
  handleInputConfirm(tag: any): void {

    this.wardTags = [tag];

  }
  handleClose(removedTag: {}): void {
    this.wardTags = this.wardTags.filter(tag => tag !== removedTag);
  }

  sliceTagName(tag: string): string {
    const isLongTag = tag.length > 20;
    return isLongTag ? `${tag.slice(0, 20)}...` : tag;
  }

  ///////////////////////////////////
  handleInputConfirmBed(tag: any): void {
    this.bedTags = [tag];

  }
  handleCloseBed(removedTag: {}): void {
    this.bedTags = this.bedTags.filter(tag => tag !== removedTag);
  }

  sliceTagNamebed(tag: string): string {
    const isLongTag = tag.length > 20;
    return isLongTag ? `${tag.slice(0, 20)}...` : tag;
  }

}

