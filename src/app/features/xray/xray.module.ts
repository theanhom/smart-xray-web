import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgZorroModule } from '../../ng-zorro.module';
import { SharedModule } from '../../shared/shared.module';
import { VariableShareService } from '../../core/services/variable-share.service';

import { XrayRoutingModule } from './xray-routing.module';
import { XrayComponent } from './xray.component';


@NgModule({
  declarations: [
    XrayComponent
  ],
  imports: [
    CommonModule,
    NgZorroModule,
    SharedModule,
    XrayRoutingModule
  ],
  providers: [VariableShareService]
})
export class XrayModule { }
