import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientListComponent } from './patient-list.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  {
    path: '',
    component: PatientListComponent,
    data: {
      breadcrumb: 'ทะเบียนผู้ป่วย'
    },
    children: [
      {
        path: '',

        component: MainComponent,
        data: {
          breadcrumb: 'ทะเบียนผู้ป่วย'
        },
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientListRoutingModule { }
