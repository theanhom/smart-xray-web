import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdmitCenterRoutingModule } from './admit-center-routing.module';
import { AdmitCenterComponent } from './admit-center.component';
import { NgZorroModule } from 'src/app/ng-zorro.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    AdmitCenterComponent
  ],
  imports: [
    CommonModule,
    AdmitCenterRoutingModule,
    NgZorroModule,
    SharedModule,
  ]
})
export class AdmitCenterModule { }
