import { Component, OnInit, VERSION } from '@angular/core';
import { ActivatedRoute, NavigationStart, NavigationExtras, Router, Navigation } from '@angular/router';
import { DateTime } from 'luxon';
import { NurseService } from '../../services/nurse.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { UserProfileService } from '../../../../core/services/user-profiles.service';
import { AxiosResponse } from 'axios';
import { LibService } from 'src/app/shared/services/lib.service';
import * as _ from 'lodash';
import { DashboardRoutingModule } from '../../../dashboard/dashboard-routing.module';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { NgxSpinnerService,Spinner } from "ngx-spinner";



@Component({
  selector: 'app-waiting-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  query: any = '';
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;

  wardId: any;
  wards: any = [];
  isLoading: boolean = true;
  appstate$: Observable<object | undefined>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private nurseService: NurseService,
    private message: NzMessageService,
    private modal: NzModalService,
    private libService: LibService,
    private userProfileService: UserProfileService,
    private spinner:NgxSpinnerService,


  ) {
    this.appstate$ = this.router.events.pipe(
      filter(e => e instanceof NavigationStart),
      map(() => {
        const currentState: Navigation | null = this.router.getCurrentNavigation();
        return currentState?.extras.state;

      })
    );
    this.user_login_name = this.userProfileService.fname;

  }

  ngOnInit() {

    // this.user_login_name  =  this.userProfileService.user_login_name;
    this.user_login_name = sessionStorage.getItem('userLoginName');
    console.log(this.user_login_name);
    this.getWard();
  }
  doSearch() {
    this.getList();
  }
  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }

  onSelectWard(event: any) {
    this.wardId = event;
    this.getList();
  }

  refresh() {
    this.query = '';
    this.pageIndex = 1;
    this.offset = 0;
    this.getList();
  }

  onPageIndexChange(pageIndex: any) {

    this.offset = pageIndex === 1 ?
      0 : (pageIndex - 1) * this.pageSize;

    this.getList()
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize
    this.pageIndex = 1

    this.offset = 0

    this.getList()
  }

  async getList() {
    this.spinner.show();
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      const response = await this.nurseService.getWaiting(_limit, _offset);
      console.log(response);

      const data: any = response.data;

      this.total = data.length || 1

      this.dataSet = data.map((v: any) => {
        const date = v.admit_date ? DateTime.fromISO(v.admit_date).setLocale('th').toLocaleString(DateTime.DATE_MED) : '';
        const time = v.admit_time ? DateTime.fromISO(v.admit_date).setLocale('th').toLocaleString(DateTime.TIME_24_SIMPLE) : '';
        v.admit_date = date;
        // v.admit_time = time;
        return v;
      });
      this.message.remove(messageId);
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
    }
  }

  async getWard() {
    try {
      const response: AxiosResponse = await this.libService.getWard();
      const responseData: any = response.data;
      const data: any = responseData.data;
      this.wards = data;
      await this.getList();
      this.isLoading = false;
    } catch (error) {
      console.log(error);
    }
  }

  registerIpt(data: any) {
    console.log(data) ;
    let dk:any = {an:data.an};
    let jsonString = JSON.stringify(dk);
    console.log(jsonString);
    this.router.navigate(['/nurse/waiting/register'], { queryParams: { data: jsonString  } });


    // จะส่ง  ค่า พารามส์แบบ ปิด  แต่ติดตรง  รีเฟรสค่า ไม่กลับมา
    // let objToSend: NavigationExtras = {
    //   queryParams: data
    // };
    // this.router.navigate(['/nurse/register'], {
    //   state: { data: objToSend }
    // });
  }
}
