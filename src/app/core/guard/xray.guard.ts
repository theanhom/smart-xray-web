import { inject } from '@angular/core';
import { CanActivateFn } from '@angular/router';
import { XrayGuardService } from 'src/app/shared/guard/xray-guard.service';

export const xrayGuard: CanActivateFn = (route, state) => {
  const authService = inject(XrayGuardService);
  return authService.isAllow();
};
