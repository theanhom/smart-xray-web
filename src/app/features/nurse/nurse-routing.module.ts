import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NurseComponent } from './nurse.component';
import { nurseWaitingGuard } from '../../core/guard/nurse-waiting.guard';

const routes: Routes = [
  {
    path: '',
    component: NurseComponent,
    children: [
      {
        path: '', redirectTo: 'patient-list', pathMatch: 'full'
      },
      {
        path: 'patient-list',
        loadChildren: () => import('./patient-list/patient-list.module').then(m => m.PatientListModule),
        data: {
          breadcrumb: 'ทะเบียนผู้ป่วย'
        },
      },
      // {
      //   path: 'waiting',
      //   canActivate: [nurseWaitingGuard],
      //   loadChildren: () => import('./waiting/waiting.module').then(m => m.WaitingModule),
      //   data: {
      //     breadcrumb: 'ผู้ป่วยรอรับเข้าตึก'
      //   },
      // },
      // {
      //   path: 'register',
      //   canActivate: [nurseWaitingGuard],
      //   loadChildren: () => import('./register/register.module').then(m => m.RegisterModule),
      //   data: {
      //     breadcrumb: 'ลงทะเบียนผู้ป่วยใน'
      //   },
      // },
    ]
  },



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NurseRoutingModule { }
