import { Component } from '@angular/core';
import { NzMenuThemeType } from 'ng-zorro-antd/menu';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-xray',
  templateUrl: './xray.component.html',
  styleUrls: ['./xray.component.css']
})
export class XrayComponent {
  isCollapsed: boolean = false;
  private subscription?: Subscription;
  user_login_name: any;
  user_id: any
  // theme: string = 'light';
  theme: NzMenuThemeType = 'light';
}
