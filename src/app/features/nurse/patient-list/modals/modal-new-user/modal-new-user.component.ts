import { NzMessageService } from 'ng-zorro-antd/message';

import { Component, EventEmitter, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';

import { ICreateUser, IUpdateUser } from '../../../../../core/model/user';
import { RandomstringService } from '../../../../../core/services/randomstring.service';
// import { UserService } from '../../services/user.service';
import * as _ from 'lodash';
import { NurseService } from '../../../services/nurse.service';
import { DateTime } from 'luxon';
import { LoginService } from '../../../../login/services/login.service';


@Component({
  selector: 'app-modal-new-user',
  templateUrl: './modal-new-user.component.html',
  styleUrls: ['./modal-new-user.component.css']
})
export class ModalNewUserComponent {
  validateForm!: UntypedFormGroup;

  @Output() onSubmit = new EventEmitter<any>();
  isOkLoading = false;
  isVisible = false;
  userId: any = null;
  query: any;
  cid: any;
  fname: any;
  lname: any;
  age: any;
  gender: any;
  phone: any;
  title: any;
  orDate: any;
  hn: any;
  diagnosis: any;
  itemData: any;
  is_active: boolean = false;
  password: any;
  itemDoctor: any;
  selectedDoctor: any = null;
  operation: any;
  constructor(
    private randomString: RandomstringService,
    // private userService: UserService,
    private message: NzMessageService,
    private nurseService: NurseService,
    private loginService: LoginService,
    private fb: UntypedFormBuilder) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      hn: [null, [Validators.required]],
      cid: [null, [Validators.required]],
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      fname: [null, [Validators.required]],
      lname: [null, [Validators.required]],
      is_active: [true],
      diagnosis: [null, [Validators.required]],
      orDate: [null, [Validators.required]],
      phone: [null, [Validators.required]],
      selectedDoctor: [null, [Validators.required]],
      operation: [null, [Validators.required]],
    });
    this.getDoctor();
  }

  showModal(data: any = ''): void {
    this.validateForm.reset()
    this.validateForm.controls['username'].enable()
    this.validateForm.controls['password'].enable()

    this.isVisible = true
    this.userId = null;

    if (data) {
      this.userId = data.id;
      this.hn = data.hn;
      this.cid = data.cid;
      this.fname = data.fname;
      this.lname = data.lname;
      this.diagnosis = data.diagnosis;
      this.phone = data.phone;
      this.is_active = data.is_active;
      this.selectedDoctor = data.doctor;
      this.operation = data.operation;
      console.log(this.userId);

    }

  }

  async getDoctor() {
    let query: any = {};
    try {
      const response = await this.nurseService.getDoctor();
      this.itemDoctor = response.data;
    } catch (error: any) {

    }
  }

  async getPatientHis() {
    this.isOkLoading = true;
    const messageId = this.message.loading('Loading...').messageId;
    try {
      if (this.validateForm.value.hn) {
        this.query = this.validateForm.value.hn;
        const response: any = await this.nurseService.getWaitingInfo(this.query);
        console.log(response.data);

        this.cid = await response.data.cardid.split("-");
        this.cid = this.cid[0] + this.cid[1] + this.cid[2] + this.cid[3] + this.cid[4];
        this.hn = await response.data.hn;
        this.title = await response.data.pttitle;
        this.fname = await response.data.ptfname;
        this.lname = await response.data.ptlname;
        this.phone = await response.data.ptphone;
        this.age = await (this.calculateAge(response.data.ptdob));

        if (response.data.ptsex == 'SX1') {
          this.gender = 'ชาย'
        } else if (response.data.ptsex == 'SX2') {
          this.gender = 'หญิง'
        }

        this.phone = await response.data.ptphone;
        this.message.remove(messageId)
      } else {
        this.isOkLoading = false
        this.message.remove(messageId);
        this.message.error(`กรุณาระบุ HN`);
      }
      this.isOkLoading = false
    } catch (error: any) {
      this.isOkLoading = false
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }

  }

  calculateAge(dateString: any) { // a date on string "22/10/1988
    let mdob: any = DateTime.fromISO(dateString).setLocale('en').toFormat('dd-MM-yyyy');
    let age: any;
    var dateParts = mdob.split("-");
    var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);

    if (dateString) {
      var timeDiff = Math.abs(Date.now() - new Date(dateObject).getTime());
      age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
    }
    return age;
  }

  async saveRegister() {
    if (this.userId) {
      this.isOkLoading = true
      const messageId = this.message.loading('กำลังแก้ไขข้อมูล...').messageId
      try {
        let data: any = {
          'hn': this.hn,
          'cid': this.validateForm.value.cid,
          'fname': this.fname,
          'lname': this.lname,
          'phone': this.phone,
          'diagnosis': this.validateForm.value.diagnosis,
          'is_active': this.validateForm.value.is_active,
          'doctor': this.selectedDoctor,
          'operation': this.operation
        }

        console.log(data);

        await this.nurseService.update(this.userId, data)
        this.message.remove(messageId)
        this.isOkLoading = false
        this.isVisible = false
        this.onSubmit.emit(true)
      } catch (error: any) {
        this.isOkLoading = false
        this.message.remove(messageId)
        this.message.error(`${error.code} - ${error.message}`)
      }
    } else {
      this.isOkLoading = true
      const messageId = this.message.loading('กำลังบันทึกข้อมูล...').messageId
      try {
        let data: any = {
          'hn': this.hn,
          'cid': this.cid,
          'title': this.title,
          'fname': this.fname,
          'lname': this.lname,
          'gender': this.gender,
          'age': this.age,
          'phone': this.phone,
          'diagnosis': this.diagnosis,
          'password': this.password,
          'is_active': this.is_active,
          'ordate': this.orDate,
          'doctor': this.selectedDoctor,
          'operation': this.operation
        }

        console.log(data);

        const roles = [{ "role": "user", "scope": ["order.read"] }];

        await this.nurseService.saveRegister(data)

        let dataUser: any = {
          'username': this.cid,
          'password': this.password,
          'is_active': true,
          'roles': JSON.stringify(roles),
        }

        console.log(dataUser);

        await this.nurseService.saveRegisterUser(dataUser)

        this.message.remove(messageId)
        this.isOkLoading = false
        this.isVisible = false
        this.onSubmit.emit(true)
      } catch (error: any) {
        this.isOkLoading = false
        this.message.remove(messageId)
        this.message.error(`${error.code} - ${error.message}`)
      }
    }
    this.clearData();
  }

  handleOk(): void {
    if (this.cid) {
      this.saveRegister();
    }
  }

  handleCancel(): void {
    this.isOkLoading = false;
    this.onSubmit.emit(false);
    this.isVisible = false;
    this.clearData();
  }

  clearData() {
    this.itemData = null;
    this.userId = null;
    this.hn = null;
    this.cid = null;
    this.fname = null;
    this.lname = null;
    this.diagnosis = null;
    this.phone = null;
    this.selectedDoctor = null;
    this.operation = null;
    this.password = null;
  }

  async doUpdate(user: IUpdateUser) {
    // this.isOkLoading = true
    // const messageId = this.message.loading('กำลังบันทึกข้อมูล...', { nzDuration: 0 }).messageId
    // try {
    //   await this.userService.update(this.userId, user)
    //   this.message.remove(messageId)
    //   this.isOkLoading = false
    //   this.isVisible = false
    //   this.onSubmit.emit(true);
    // } catch (error: any) {
    //   this.isOkLoading = false
    //   this.message.remove(messageId);
    //   this.message.error(`${error.code} - ${error.message}`);
    // }
  }

  randomPassword() {
    const randomPassword = this.randomString.generateRandomString();
    this.validateForm.patchValue({ password: randomPassword });
  }

  onChangeMainOperator(event: any) {
    // this.getOperators(event);
  }

  async getUserInfo(id: any) {
    // const messageId = this.message.loading('Loading...').messageId;
    // try {
    //   const response: any = await this.userService.info(id);
    //   const user = response.data;

    //   this.validateForm.patchValue({
    //     username: user.username,
    //     firstName: user.first_name,
    //     lastName: user.last_name,
    //     isAdmin: _.includes(user.role, 'ADMIN') ? true : false,
    //     enabled: user.enabled,
    //   })

    //   // this.getOperators(user.main_operator_id)
    //   // patch value
    //   this.validateForm.patchValue({
    //     hospcode: user.hospcode,
    //   })

    //   this.validateForm.controls['username'].disable()
    //   this.validateForm.controls['password'].disable()

    //   this.message.remove(messageId)
    // } catch (error: any) {
    //   this.message.remove(messageId);
    //   this.message.error(`${error.code} - ${error.message}`);
    // }
  }

}
