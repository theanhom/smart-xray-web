import { TestBed } from '@angular/core/testing';

import { XrayGuardService } from './xray-guard.service';

describe('XrayGuardService', () => {
  let service: XrayGuardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(XrayGuardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
