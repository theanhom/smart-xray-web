import { NzMessageService } from 'ng-zorro-antd/message';
import { Component, EventEmitter, Output } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { RandomstringService } from '../../../../../core/services/randomstring.service';
import * as _ from 'lodash';
import { NurseService } from '../../../services/nurse.service';
import { DateTime } from 'luxon';

@Component({
  selector: 'app-modal-view-user',
  templateUrl: './modal-view-user.component.html',
  styleUrls: ['./modal-view-user.component.css']
})
export class ModalViewUserComponent {
  validateForm!: UntypedFormGroup;

  @Output() onSubmit = new EventEmitter<any>();
  isOkLoading = false;
  isVisible = false;
  hn: any;
  userId: any = null;
  itemDataContent: any;
  itemData: any;

  constructor(
    private randomString: RandomstringService,
    private message: NzMessageService,
    private nurseService: NurseService,
    private fb: UntypedFormBuilder) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      // hn: [null, [Validators.required]],
    });
  }

  showModal(data: any = ''): void {
    this.validateForm.reset()
    // this.validateForm.controls['hn'].enable()
    console.log(data);

    this.isVisible = true
    this.userId = null;
    if (data) {
      this.itemData = data;
      this.itemDataContent = data.title + data.fname + '  ' + data.lname
      console.log(this.itemDataContent);
    }
  }

  handleOk(): void {
    console.log('OK');
    this.isOkLoading = false;
    this.onSubmit.emit(false);
    this.isVisible = false;
  }

  handleCancel(): void {
    this.isOkLoading = false;
    this.onSubmit.emit(false);
    this.isVisible = false;
  }

}
