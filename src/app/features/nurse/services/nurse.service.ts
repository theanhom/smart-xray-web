import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NurseService {
  // pathPrefixLookup: any = `:40013/lookup`
  // pathPrefixNurse: any = `:40012/nurse`
  // pathPrefixAuth: any = `:40010/auth`

  pathPrefixLookup: any = environment.pathPrefixLookup;
  pathPrefixNurse: any = environment.pathPrefixNurse;
  pathPrefixAuth: any = environment.pathPrefixAuth;

  private axiosInstance = axios.create({
    // baseURL: `${environment.apiUrl}${this.pathPrefixNurse}`
    baseURL: `${this.pathPrefixNurse}`
  })

  constructor() {
    this.axiosInstance.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token')
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
      }
      return config
    });

    this.axiosInstance.interceptors.response.use(response => {
      return response
    }, error => {
      return Promise.reject(error)
    })
  }

  async getWaitingInfo(hn: any) {
    const url = `/his-services/waiting-info?hn=${hn}`
    return await this.axiosInstance.get(url)
  }

  async saveRegister(data: object) {
    return await this.axiosInstance.post('/patient', data)
  }

  async getPatient() {
    const url = `/patient/getpatient`
    console.log(url);
    return await this.axiosInstance.get(url)
  }

  async getDoctor() {
    const url = `/his-services/getDoctor`
    console.log(url);
    return await this.axiosInstance.get(url)
  }

  async getPatientByHn(hn: string) {
    const url = `/patient/getpatientbyhn/${hn}`
    console.log(url);
    return await this.axiosInstance.get(url)
  }

  async getTotalInfo(hn: string) {
    const url = `/patient/gettotalinfo/${hn}`
    console.log(url);
    return await this.axiosInstance.get(url)
  }

  async changePassword(id: any, password: any) {
    return await this.axiosInstance.put(`/patient/${id}/change-password`, {
      password
    })
  }

  async update(id: any, data: any) {
    return await this.axiosInstance.put(`/patient/${id}`, {
      data
    })
  }

  async saveRegisterUser(data: object) {
    return await this.axiosInstance.post('/patient/register', data)
  }

  async saveRegisterProfile(data: object) {
    return await this.axiosInstance.post('/patient/registerProfile', data)
  }

}
