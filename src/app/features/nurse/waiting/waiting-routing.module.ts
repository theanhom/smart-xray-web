import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {} from './main/main.component';
import { WaitingComponent } from './waiting.component';
import {RegistersComponent} from './register/register.component'
import {MainComponent} from './main/main.component';

const routes: Routes = [
  {
    path: '',
    component: WaitingComponent,
    data: {
      breadcrumb: 'รอรับเข้าตึก'
    },
    children:[
      {
        path: '',
       
        component: MainComponent,
        data: {
          breadcrumb: 'ลงทะเบียนผู้ป่วยใน'
        },
      },
       {
        path: 'register',
       
        component: RegistersComponent,
        data: {
          breadcrumb: 'ลงทะเบียนผู้ป่วยใน'
        },
      },
    ]

  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WaitingRoutingModule { }
